#ifndef _VECTOR_H_
#define _VECTOR_H_
#include <iostream>
#include <initializer_list>

using namespace std;

class Vector{
protected :
int n;
double* vect;
public :
Vector();
Vector(int i);
Vector(initializer_list<double> ar);
Vector(const Vector & v);
~Vector(){delete[] vect;}
int getdim();
void Redim(int i);
double norm();
const Vector & operator= (const Vector & v);
const Vector & operator= (initializer_list<double> ar);        
Vector operator+ (const Vector & v) const; 
Vector operator- (const Vector & v) const; 
Vector operator^ (const Vector & v) const; //cross product
double  operator* (const Vector & v) const;
const Vector& operator+=(const Vector & v); 
const Vector& operator-=(const Vector & v); 
const Vector& operator*=(double a); 
const Vector& operator/=(double a); 
friend Vector operator/ (const Vector & v,double a); 
friend Vector operator* ( const Vector & v, double a); 
friend Vector operator^ (const Vector & v, double a); //pow
friend ostream & operator<< (ostream & out,const  Vector & v);
double & operator() (int i);
  double operator() (int i) const;

};


#endif 
