#include "Vector.h"
#include <iomanip>
#include <cmath>
#include <cassert>
using namespace std;


Vector::Vector()
{
	vect=new double[1];
	n=1;
	vect[0]=0;
}
Vector::Vector(int i)
{
	vect=new double[i];
	n=i;
	for(int i=0;i<n;i++)
		vect[i]=0;	
}
Vector::Vector(initializer_list<double> ar)
{
	n=ar.size();
	vect = new double[n];
	
	for(int i=0;i<n;i++)
		vect[i]=*(ar.begin()+i);
	
}
Vector::Vector(const Vector & v)
{
	n=v.n;
	vect=new double[n];
	for(int i=0;i<n;i++)
		vect[i]=v.vect[i];
}

int Vector::getdim()
{
	return n;
}

double Vector::norm()
{
	return sqrt( (*this) * (*this) );
}

double & Vector:: operator() (int i) {
    assert(i >= 0 && i <= n-1);
    double * ptr =& vect[i];
    return *ptr;
  } // read and write

double Vector:: operator() (int i) const {
    assert(i >= 0 && i <= n-1);
    return vect[i];
  } // read only

void Vector::Redim(int i)
{
	if(n==i)
		return;
	else
	{
		delete[] vect;
		n=i;
		vect=new double[n];
		for(int i=0;i<n;i++)
			vect[i]=0;
	}
}

Vector Vector::operator+ (const Vector & v) const {
	assert(n==v.n);
	Vector Vp(n);
	for(int i=0;i<n;i++)
		Vp(i)=vect[i]+v.vect[i];
	return Vp;
}

Vector Vector::operator- (const Vector & v) const {
	assert(n==v.n);
	Vector Vp(n);
	for(int i=0;i<n;i++)
		Vp(i)=vect[i]-v.vect[i];
	return Vp;
}

double Vector::operator* (const Vector & v) const {
	assert(n==v.n);
	double temp=0;
	for(int i=0;i<n;i++)
		temp+=vect[i]*v.vect[i];
	return temp;
}
Vector Vector::operator^ (const Vector & v) const {
	assert(n==3 && v.n==3);
	Vector Vp(n);
	Vp(0)=vect[1]*v.vect[2]-vect[2]*v.vect[1];
	Vp(1)=vect[2]*v.vect[0]-vect[0]*v.vect[2];
	Vp(2)=vect[0]*v.vect[1]-vect[1]*v.vect[0];
	return Vp;
}
const Vector& Vector::operator+=(const Vector & v)
{
	assert(n==v.n);
	for(int i=0;i<n;i++)
		vect[i]+=v(i);
	return (*this);
}
const Vector& Vector::operator-=(const Vector & v)
{
	assert(n==v.n);
	for(int i=0;i<n;i++)
		vect[i]-=v(i);
	return (*this);
}
const Vector& Vector::operator*=(double a)
{
	for(int i=0;i<n;i++)
		vect[i]*=a;
	return (*this);
}
const Vector& Vector::operator/=(double a)
{
	for(int i=0;i<n;i++)
		vect[i]/=a;
	return (*this);
}
const Vector & Vector::operator= (const Vector & v)
{
	if (v.n != n) 
	{ 
		this->Redim(v.n);
	}
  	for(int i=0;i<v.n;i++)
		vect[i]=v(i);

	return (*this); // On renvoie aussi le résultat de l'affectation
}  

const Vector & Vector::operator=(initializer_list<double> ar)
{

	int nar=ar.size();
	if(nar != n)
		this->Redim(nar);
	for(int i=0;i<n;i++)
		vect[i]=*(ar.begin()+i);

	return (*this);
}

ostream & operator<< (ostream & out, const Vector & v)
{
	int n=v.n;
	out << "( ";	
	for (int i = 0; i < n; i++)
		out << setw(8) << setprecision(6) << v(i) << " ";
	out << ")";
	return out; 

}


Vector operator/ (const Vector & v,double a)
{
	Vector Vb=v;
	for(int i=0;i<Vb.getdim();i++)
		Vb(i)/=a;

	return Vb;
}

Vector operator* (const Vector & v,double a)
{
	Vector Vb=v;
	for(int i=0;i<Vb.getdim();i++)
		Vb(i)*=a;

	return Vb;
}
Vector operator^ (const Vector & v,double a)
{
	Vector Vb=v;
	for(int i=0;i<Vb.getdim();i++)
		Vb(i)=pow(Vb(i),a);

	return Vb;
}

