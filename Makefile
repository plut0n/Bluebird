#Standard Rules
dir :=./libVector ./Bluebird ./CatBird
.PHONY: all $(dir)
.PHONY: clean
.PHONY: clean-eastwood
#EXE+LIB
all : $(dir)
#clean
clean : 
	-for psykokwak in $(dir); do (cd $$psykokwak; $(MAKE) clean ); done
clean-eastwood:
	-for psykokwak in $(dir); do (cd $$psykokwak; $(MAKE) clean-all ); done
#program + lib in dir
$(dir):
	$(MAKE) -C $@


