#include "cam.gouda"
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <cmath>
#include "sdlglutils.gouda"

Cam::Cam(double dist)
{
	r=50;
    const char *protohandopen[] =
        {
            /* width height num_colors chars_per_pixel */
            " 16 16 3 1 ",
            /* colors */
            "X c #000000",
            ". c #ffffff",
            "  c None",
            /* pixels */
            "       XX       ",
            "   XX X..XXX    ",
            "  X..XX..X..X   ",
            "  X..XX..X..X X ",
            "   X..X..X..XX.X",
            "   X..X..X..X..X",
            " XX X.......X..X",
            "X..XX..........X",
            "X...X.........X ",
            " X............X ",
            "  X...........X ",
            "  X..........X  ",
            "   X.........X  ",
            "    X.......X   ",
            "     X......X   ",
            "     X......X   ",
            "0,0"
        };

    const char *protohandclose[] =
        {
            /* width height num_colors chars_per_pixel */
            " 16 16 3 1 ",
            /* colors */
            "X c #000000",
            ". c #ffffff",
            "  c None",
            /* pixels */
            "                ",
            "                ",
            "                ",
            "                ",
            "    XX XX XX    ",
            "   X..X..X..XX  ",
            "   X........X.X ",
            "    X.........X ",
            "   XX.........X ",
            "  X...........X ",
            "  X...........X ",
            "  X..........X  ",
            "   X.........X  ",
            "    X.......X   ",
            "     X......X   ",
            "     X......X   ",
            "0,0"
        };
    handopen = cursorFromXPM(protohandopen);
    handclose = cursorFromXPM(protohandclose);
    SDL_SetCursor(handopen);
    click = false;
    theta = 0;
    phi = 0;
    r = dist;
    motionSensivity = 0.3;
    scrollSensivity = 1;
}

void Cam::mousemotion(const SDL_MouseMotionEvent & event)
{
    if (click)
    {
        phi += event.xrel*motionSensivity;
        theta += event.yrel*motionSensivity;
        if (theta > 90)
            theta = 90;
        else if (theta < -90)
            theta = -90;
    }
}

void Cam::rightarrow()
{ 
	phi+=2;
}
void Cam::leftarrow()
{ 
	phi-=2;
}
void Cam::uparrow()
{ 
                theta += 2;
        if (theta > 90)
            theta = 90;
        else if (theta < -90)
            theta = -90;
}
void Cam::downarrow()
{ 
         theta -= 2;
        if (theta > 90)
            theta = 90;
        else if (theta < -90)
            theta = -90;
}
void Cam::plus()
{ 
        r -= 1;
        if (r < 0.1)
            r = 0.1;
}
void Cam::minus()
{ 
        r += 1;
}
void Cam::mouseclick(const SDL_MouseButtonEvent & event)
{
    if (event.button == SDL_BUTTON_LEFT)
    {
        if ((click)&&(event.type == SDL_MOUSEBUTTONUP))
        {
            click = false;
            SDL_SetCursor(handopen);
        }
        else if ((!click)&&(event.type == SDL_MOUSEBUTTONDOWN))
        {
            click = true;
            SDL_SetCursor(handclose);
        }
    }
    else if ((event.button == SDL_BUTTON_WHEELUP)&&(event.type == SDL_MOUSEBUTTONDOWN))
    {
        r -= scrollSensivity;
        if (r < 0.1)
            r = 0.1;
    }
    else if ((event.button == SDL_BUTTON_WHEELDOWN)&&(event.type == SDL_MOUSEBUTTONDOWN))
    {
           r += scrollSensivity;
    }
}

void Cam::setMotionSensivity(double sensivity)
{
    motionSensivity = sensivity;
}

void Cam::setScrollSensivity(double sensivity)
{
    scrollSensivity = sensivity;
}


Cam::~Cam()
{
    SDL_FreeCursor(handopen);
    SDL_FreeCursor(handclose);
    SDL_SetCursor(NULL);
}

void Cam::gogogo()
{

   gluLookAt(r,0,0,0,0,0,0,0,1);
    glRotated(theta,0,1,0);
    glRotated(phi,0,0,1);
 
}

